import React from 'react';
import { configure, shallow} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import BookingEdit from './components/BookingEdit'
configure({adapter: new Adapter()});

describe("Testing BookingEdit component existance", () =>{

    it("The form of booking edit exists" ,  () => {
        const wrapper= shallow(<BookingEdit/>);
        expect(wrapper.find(".container").find(".cart").length==1).toBe(true);
    });

});