import React from 'react';
import { configure, shallow} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import Home from './components/Home'



configure({adapter: new Adapter()});

const items=[
    {
      "id": 1,
      "title": "Pho",
      "description": "Pho is essentially Vietnam’s signature dish, comprising rice noodles in a flavourful soup with meat and various greens, plus a side of nuoc cham (fermented fish) or chilli sauce",
      "status": "Available",
      "price": 10,
      "image": "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/0/image.jpg"
    },
    {
      "id": 2,
      "title": "Banh Mi",
      "description": "Banh mi is a unique French-Vietnamese sandwich that’s great for when you’re in need of a quick meal",
      "status": "Available",
      "price": 15,
      "image": "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/00/image.jpg"
    },
    {
      "id": 3,
      "title": "Banh Xeo (Crispy Pancake)",
      "description": "Similar to a crepe or pancake, banh xeo is made of rice flour, coconut milk, and turmeric, which you can fill it with vermicelli noodles, chicken, pork or beef slices, shrimps, sliced onions, beansprouts, and mushrooms",
      "status": "Available",
      "price": 10,
      "image": "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/01/image.jpg"
    },
    {
      "id": 4,
      "title": "Goi Cuon (Vietnamese Fresh Spring Rolls)",
      "description": "Goi cuon (Vietnamese fresh spring rolls) consist of thin vermicelli noodles, pork slices, shrimp, basil, and lettuce, all tightly wrapped in translucent banh trang (rice papers)",
      "status": "Available",
      "price": 5,
      "image": "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/02/image.jpg"
    },
    {
      "id": 5,
      "title": "Mi Quang (Vietnamese Turmeric Noodles)",
      "description": "Mi quang may be available at most restaurants in Vietnam, but it actually originates from Da Nang",
      "status": "Available",
      "price": 20,
      "image": "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/03/image.jpg"
    },
    {
      "id": 6,
      "title": "Bun Thit Nuong (Vermicelli Noodles With Grilled Pork)",
      "description": "Bun thit nuong comprises thin vermicelli rice noodles, chopped lettuce, sliced cucumber, bean sprouts, pickled daikon, basil, chopped peanuts, and mint, topped with grilled pork shoulder",
      "status": "Available",
      "price": 20,
      "image": "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/04/image.jpg"
    }
  ];
describe("Testing Home component existance", () =>{

    it("The box contains items exists" ,  () => {
        const wrapper= shallow(<Home items={items} isLoading={false} />);
        expect(wrapper.find(".container").find(".box").length==1).toBe(true);
    });
});