import React from 'react';
import { configure, shallow} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import Item from './components/Item'
configure({adapter: new Adapter()});

describe("Testing Item component existance", () =>{

    it("The item component exists" ,  () => {
        const wrapper= shallow(<Item/>);
        expect(wrapper.find(".container").find(".cart").length).toBe(1);
    });
});