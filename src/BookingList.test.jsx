import React from 'react';
import { configure, shallow} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import BookingList from './components/BookingList'

configure({adapter: new Adapter()});

const bookings=[
    {
      "id": 1,
      "customer_name": "Huiop",
      "customer_address": "fvcvcxvx",
      "customer_phone": "cvxvcxv",
      "status": "cvxvxvxv",
      "price": 25,
      "time": "25-04-2020 10:07:15",
      "items": [
        {
          "id": 1,
          "title": "Pho",
          "description": "Pho is essentially Vietnam’s signature dish, comprising rice noodles in a flavourful soup with meat and various greens, plus a side of nuoc cham (fermented fish) or chilli sauce",
          "status": "Available",
          "price": 10,
          "image": "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/0/image.jpg"
        },
        {
          "id": 2,
          "title": "Banh Mi",
          "description": "Banh mi is a unique French-Vietnamese sandwich that’s great for when you’re in need of a quick meal",
          "status": "Available",
          "price": 15,
          "image": "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/00/image.jpg"
        }
      ]
    },
    {
      "id": 2,
      "customer_name": "Huy",
      "customer_address": "fddsfg",
      "customer_phone": "fgdgfd",
      "status": "gfdgd",
      "price": 15,
      "time": "25-04-2020 10:07:15",
      "items": [
        {
          "id": 3,
          "title": "Banh Xeo (Crispy Pancake)",
          "description": "Similar to a crepe or pancake, banh xeo is made of rice flour, coconut milk, and turmeric, which you can fill it with vermicelli noodles, chicken, pork or beef slices, shrimps, sliced onions, beansprouts, and mushrooms",
          "status": "Available",
          "price": 10,
          "image": "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/01/image.jpg"
        },
        {
          "id": 4,
          "title": "Goi Cuon (Vietnamese Fresh Spring Rolls)",
          "description": "Goi cuon (Vietnamese fresh spring rolls) consist of thin vermicelli noodles, pork slices, shrimp, basil, and lettuce, all tightly wrapped in translucent banh trang (rice papers)",
          "status": "Available",
          "price": 5,
          "image": "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/02/image.jpg"
        }
      ]
    },
    {
      "id": 3,
      "customer_name": "Huycvxvcx",
      "customer_address": "fddsfg",
      "customer_phone": "fgdgfd",
      "status": "gfdgd",
      "price": 40,
      "time": "25-04-2020 10:07:15",
      "items": [
        {
          "id": 5,
          "title": "Mi Quang (Vietnamese Turmeric Noodles)",
          "description": "Mi quang may be available at most restaurants in Vietnam, but it actually originates from Da Nang",
          "status": "Available",
          "price": 20,
          "image": "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/03/image.jpg"
        },
        {
          "id": 6,
          "title": "Bun Thit Nuong (Vermicelli Noodles With Grilled Pork)",
          "description": "Bun thit nuong comprises thin vermicelli rice noodles, chopped lettuce, sliced cucumber, bean sprouts, pickled daikon, basil, chopped peanuts, and mint, topped with grilled pork shoulder",
          "status": "Available",
          "price": 20,
          "image": "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/04/image.jpg"
        }
      ]
    }
  ];
describe("Testing BookingList component existance", () =>{

    it("The table of bookings exists" ,  () => {
        const wrapper= shallow(<BookingList bookings={bookings} isLoading={false} />);
        expect(wrapper.find("div").find(".mt-4").length==1).toBe(true);
    });

    it("The table of bookings has 3 rows" ,  () => {
      const wrapper= shallow(<BookingList bookings={bookings} isLoading={false} />);
      expect(wrapper.find("div").find(".mt-4").find("tbody").find("tr").length==3).toBe(true);
  });
});