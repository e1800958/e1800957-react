import Home from './components/Home'
import React, { Component } from 'react';

class fetchItems extends Component{

    constructor(props) {
        super(props);
        this.state = {items: [], isLoading: true};
    }

    componentDidMount() {
        this.setState({isLoading: true});
    
        fetch('https://booking-product.herokuapp.com/items',{
          method: "GET",
          headers: {
            Accept: "application/json",
            Authorization: "Basic " + btoa("user:password123"),
          },
        })
           .then(response => response.json())
          .then(data => this.setState({items: data, isLoading: false}));
      }
    
    handleClick = (id)=>{
        this.props.addToCart(id); 
    }

    render(){
        const {items, isLoading} = this.state;
        if (isLoading) {
            return <p>Loading...</p>;
        }

        return(
            <Home items={items} isLoading={isLoading} />
        )
    }
}

export default fetchItems;

