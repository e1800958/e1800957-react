import React from 'react';
import { Link } from 'react-router-dom'
 const Navbar = ()=>{
    return(
            <nav className="nav-wrapper">
                <div className="container">
                    <Link to="/" className="brand-logo">Food</Link>                  
                    <ul className="right">
                        <li><Link to="/">Manage Items</Link></li>
                        <li><Link to="/bookings">Manage Booking</Link></li>
                    </ul>
                </div>
            </nav>
   
        
    )
}

export default Navbar;