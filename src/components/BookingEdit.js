import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'

class BookingEdit extends Component{
    emptyBooking = {
        id: '',
        customer_name: '',
        customer_address: '',
        customer_phone: '',
        status: '',
        price: '',
        time: '',
        items: ''
      };

    constructor(props) {
        super(props);
        this.state = {
            booking: this.emptyBooking
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
    
        if (this.props.match.params.id !== 'new') {
            console.log(this.props.match.params.id)
            const booking_ = await (await fetch(`https://booking-product.herokuapp.com/booking/${this.props.match.params.id}`,{
            method: "GET",
            headers: {
                Accept: "application/json",
                Authorization: "Basic " + btoa("user:password123"),
            },
            })).json();
            this.setState({booking: booking_});
        }
    }
        
        
    async remove(id) {
        let updatedItems = [...this.state.booking.items].filter(i => i.id !== id);
        let item_=this.state.booking.items.filter(i => i.id === id);
        let updatedBooking = {
            id: this.state.booking.id,
            customer_name: this.state.booking.customer_name,
            customer_address: this.state.booking.customer_address,
            customer_phone: this.state.booking.customer_phone,
            status: this.state.booking.status,
            price: this.state.booking.price-item_[0].price,
            time: this.state.booking.time,
            items: updatedItems
        };;
        this.setState({booking: updatedBooking});
        // console.log(this.state.booking);
    }
        

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const id = target.id;
        let item = {...this.state.booking};
        item[id] = value;
        this.setState({item});
    }
    
    async handleSubmit() {
        const {booking}= this.state;
        // console.log(booking);
        await fetch('https://booking-product.herokuapp.com/booking/', {
            method:  'PUT' ,
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            Authorization: "Basic " + btoa("user:password123"),
            },
            body: JSON.stringify(booking),
        });
        
        this.props.history.push('/bookings');
    }

    render(){
        const {booking} = this.state;     
        let addedItems = booking.items.length ?
            (  
                booking.items.map(item=>{
                    return(
                       
                        <li className="collection-item avatar" key={item.id}>
                                    <div className="item-img"> 
                                        <img src={item.image} alt={item.image} className=""/>
                                    </div>
                                
                                    <div className="item-desc">
                                        <span className="title">{item.title}</span>
                                        <p>{item.description}</p>
                                        <p><b>Price: {item.price}$</b></p> 
                                        {/* <p>
                                            <b>Quantity: {item.quantity}</b> 
                                        </p> */}
                                        <button className="waves-effect waves-light btn pink remove" onClick={() => this.remove(item.id)}>Remove</button>
                                    </div>
                                    
                                </li>
                         
                    )
                })
            ):

            (
            <p>Nothing.</p>
            )
       return(
            <div className="container">
                <div className="cart">
                    <h5>Item</h5>
                    <ul className="collection">
                        {addedItems}
                    </ul>
                </div> 
                <div className="collection">
                    <li className="collection-item">
                        <label>
                            <input type="checkbox" ref="shipping" onChange= {this.handleChecked} />
                        </label>
                    </li>
                    <li className="collection-item"><b>Total: {this.state.booking.price} $</b></li>
                </div>
                <div className="checkout">
                    <button className="waves-effect waves-light btn" onClick={() => this.handleSubmit()}>Save</button>
                </div>          
            </div>
       )
    }
}

export default BookingEdit;