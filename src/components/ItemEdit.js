import React, { Component } from 'react';
import { withRouter,Link } from 'react-router-dom';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';

class ItemEdit extends Component{
    emptyItem = {
        id: '',
        title: '',
        description: '',
        price: '',
        image: '',
      };

    constructor(props) {
        super(props);
        this.state = {
            item: this.emptyItem
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
    
        if (this.props.match.params.id !== 'new') {
            console.log(this.props.match.params.id)
            const item_ = await (await fetch(`https://booking-product.herokuapp.com/item/${this.props.match.params.id}`,{
            method: "GET",
            headers: {
                Accept: "application/json",
                Authorization: "Basic " + btoa("user:password123"),
            },
            })).json();
            this.setState({item: item_});
        }
    }
        
        
    // async remove(id) {
    //     let updatedItems = [...this.state.bookitems].filter(i => i.id !== id);
    //     let item_=this.state.booking.items.filter(i => i.id === id);
    //     let updatedBooking = {
    //         id: this.state.booking.id,
    //         customer_name: this.state.booking.customer_name,
    //         customer_address: this.state.booking.customer_address,
    //         customer_phone: this.state.booking.customer_phone,
    //         status: this.state.booking.status,
    //         price: this.state.booking.price-item_[0].price,
    //         time: this.state.booking.time,
    //         items: updatedItems
    //     };;
    //     this.setState({booking: updatedBooking});
    //     // console.log(this.state.booking);
    // }
        

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const id = target.id;
        let item = {...this.state.item};
        item[id] = value;
        this.setState({item});
    }
    
    async handleSubmit(event) {
        event.preventDefault();
        const {item}= this.state;
        // console.log(booking);
        await fetch('https://booking-product.herokuapp.com/item/', {
            method:  'PUT' ,
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            Authorization: "Basic " + btoa("user:password123"),
            },
            body: JSON.stringify(item),
        });
        
        this.props.history.push('/');
    }

    render(){
        const {item} = this.state;     
       return(
            <div className="container">
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for="title">Title</Label>
                        <Input type="text" name="title" id="title" value={item.title || ''}
                            onChange={this.handleChange} autoComplete="title"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="description">Description</Label>
                        <textarea type="text" name="description" id="description" value={item.description || ''}
                            onChange={this.handleChange} autoComplete="description"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="price">Price</Label>
                        <Input type="text" name="price" id="price" value={item.price || ''}
                            onChange={this.handleChange} autoComplete="price"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="image">Image</Label>
                        <textarea type="text" name="image" id="image" value={item.image || ''}
                            onChange={this.handleChange} autoComplete="image"/>
                    </FormGroup>
                    <FormGroup>
                        <Button color="primary" type="submit">Save</Button>{' '}
                        <Button color="secondary" tag={Link} to="/bookings">Cancel</Button>
                    </FormGroup>
                </Form>          
            </div>
       )
    }
}

export default ItemEdit;