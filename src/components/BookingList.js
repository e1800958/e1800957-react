import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import { Link } from 'react-router-dom';


class BookingList extends Component {

    constructor(props) {
      super(props);
      this.state = {bookings: this.props.bookings, isLoading: this.props.isLoading};
      // this.remove = this.remove.bind(this);
    }
  
    // componentDidMount() {
    //   this.setState({isLoading: true});
  
    //   fetch('http://localhost:8080/bookings',{
    //     method: "GET",
    //     headers: {
    //       Accept: "application/json",
    //       Authorization: "Basic " + btoa("user:password123"),
    //     },
    //   })
    //      .then(response => response.json())
    //     .then(data => this.setState({bookings: data, isLoading: false}));
    // }
  
    // async remove(id) {
    //   await fetch('/booking/'+id, {
    //     method: 'DELETE',
    //     headers: {
    //       'Accept': 'application/json',
    //       Authorization: "Basic " + btoa("user:password123"),
    //     }
    //   }).then(() => {
    //     let updatedBookings = [...this.state.bookings].filter(i => i.id !== id);
    //     this.setState({bookings: updatedBookings});
    //   });
    // }
  
    render() {
      const {bookings, isLoading} = this.state;
  
      if (isLoading) {
        return <p>Loading...</p>;
      }
  
      const bookingList = bookings.map(booking => {
        return <tr key={booking.id}>
          <td style={{whiteSpace: 'nowrap'}}>{booking.id}</td>
          <td style={{whiteSpace: 'nowrap'}}>{booking.customer_name}</td>
          <td style={{whiteSpace: 'nowrap'}}>{booking.customer_address}</td>
          <td style={{whiteSpace: 'nowrap'}}>{booking.customer_phone}</td>
          <td style={{whiteSpace: 'nowrap'}}>{booking.items.map(item => {
              return ( <p> <span className="card-title"><Link to={"/item/"+item.id} className="title">{item.title}</Link></span></p>)
          })}</td>
          
          <td style={{whiteSpace: 'nowrap'}}>{booking.price}</td>
          <td style={{whiteSpace: 'nowrap'}}>{booking.time}</td>
          <td style={{whiteSpace: 'nowrap'}}> <b>{booking.status}</b></td>
          
          <td>
            <ButtonGroup>
              <Button size="sm" color="primary" tag={Link} to={"/booking-edit/" + booking.id}>Edit</Button>
            </ButtonGroup>
          </td>
        </tr>
      });
  
      return (
        <div>
          <div className="contain" >
            <h3>Booking List</h3>
            <Table className="mt-4">
              <thead>
              <tr>
                <th width="20%">Id</th>
                <th width="20%">Name</th>
                <th width="20%">Address</th>
                <th width="20%">Phone</th>
                <th width="20%">Items</th>
                <th width="20%">Total Fee</th>
                <th width="20%">Time</th>
                <th width="20%">Status</th>
                <th width="20%">Action</th>
              </tr>
              </thead>
              <tbody>
              {bookingList}
              </tbody>
            </Table>
          </div>
        </div>
      );
    }
  }
  
  export default BookingList;