import React, { Component } from 'react';
import { Link,  withRouter } from 'react-router-dom'


class Item extends Component{
    emptyItem = {
        id: '',
        title: '',
        description: '',
        status: '',
        price: '',
        image: '',
      };

    constructor(props) {
    super(props);
    this.state = {
        item: this.emptyItem
    };
    this.handleChange = this.handleChange.bind(this);
    }

    async componentDidMount() {
        
        if (this.props.match.params.id !== 'new') {
            console.log(this.props.match.params.id)
          const item_ = await (await fetch(`https://booking-product.herokuapp.com/item/${this.props.match.params.id}`,{
            method: "GET",
            headers: {
              Accept: "application/json",
              Authorization: "Basic " + btoa("user:password123"),
            },
          })).json();
          this.setState({item: item_});
        }
      }

      handleChange(event) {
        const target = event.target;
        const value = target.value;
        const id = target.id;
        let item = {...this.state.item};
        item[id] = value;
        this.setState({item});
      }

      async handleSubmit(event) {
        event.preventDefault();
        const {item} = this.state;
    
        await fetch('https://booking-product.herokuapp.com/item', {
          method: (item.id) ? 'PUT' : 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            Authorization: "Basic " + btoa("user:password123"),
          },
          body: JSON.stringify(item),
        });
        this.props.history.push('/');
      }

    render(){
        const {item} = this.state;
       return(
            <div className="container">
                <div className="cart">
                    <h5>Item</h5>
                    <ul className="collection">
                    <li className="collection-item avatar" key={item.id}>
                                    <div className="item-img"> 
                                        <img src={item.image} alt={item.image} className=""/>
                                    </div>
                                
                                    <div className="item-desc">
                                        <span className="title">{item.title}</span>
                                        <p>{item.description}</p>
                                        <p><b>Price: {item.price}$</b></p> 
                                        <p>
                                            <b>Status: {item.status}</b> 
                                        </p>
                                        <Link to={'/item-edit/' + item.id}>
                                            <button className="waves-effect waves-light btn pink remove">
                                                Edit Item
                                            </button>
                                        </Link>
                                    </div>
                                    
                        </li>
                    </ul>
                </div> 
            </div>
       )
    }
}
export default Item