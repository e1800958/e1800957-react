import React from 'react';
import { configure, shallow} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import ItemEdit from './components/ItemEdit';

configure({adapter: new Adapter()});

describe("Testing Item component existance", () =>{

    it("The box contains items exists" ,  () => {
        const wrapper= shallow(<ItemEdit/>);
        expect(wrapper.find(".container").find("Form").length).toBe(1);
    });
});