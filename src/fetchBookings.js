import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import BookingList from './components/BookingList'


class fetchBookings extends Component {

    constructor(props) {
      super(props);
      this.state = {bookings: [], isLoading: true};
    }
  
    componentDidMount() {
      this.setState({isLoading: true});
  
      fetch('https://booking-product.herokuapp.com/bookings',{
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: "Basic " + btoa("user:password123"),
        },
      })
         .then(response => response.json())
        .then(data => this.setState({bookings: data, isLoading: false}));
    }
  

  
    render() {
      const {bookings, isLoading} = this.state;
  
      if (isLoading) {
        return <p>Loading...</p>;
      }
  
      
  
      return (
        <BookingList bookings= {bookings} isLoading={isLoading}/>
      );
    }
  }
  
  export default fetchBookings;