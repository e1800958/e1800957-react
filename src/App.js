import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import Navbar from './components/Navbar'
import fetchItems from './fetchItems'
import fetchBookings from './fetchBookings'
import BookingEdit from './components/BookingEdit'
import Item from './components/Item'
import ItemEdit from './components/ItemEdit'

class App extends Component {
  render() {
    return (
       <BrowserRouter>
            <div className="App">
            
              <Navbar/>
                <Switch>
                    {/* <Route exact path="/" component={Home}/> */}
                    <Route exact path="/" component={fetchItems}/>
                    <Route path="/booking-edit/:id" component={BookingEdit}/>
                    <Route path="/item/:id" component={Item}/>
                    {/* <Route path="/bookings" component={BookingList}/> */}
                    <Route path="/bookings" component={fetchBookings}/>
                    <Route path="/item-edit/:id" component={ItemEdit}/>
                  </Switch>
             </div>
       </BrowserRouter>
      
    );
  }
}

export default App;
